package com.somosrenka.developers.functions.JSONParsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonDatosCarro
{
    public JSONObject jObject;
    private boolean isEmpy = false;

    public JsonDatosCarro(String json)
    {
        try
        {
            jObject = new JSONObject(json);
            this.isEmpy=false;
        }
        catch(JSONException e)
        {
            isEmpy=true;
        }
    }

    public String getToken() throws JSONException
    { return jObject.getString("key"); }

    public String getError() throws JSONException
    { return jObject.optString("detail"); }

    public List<String> getErrors() throws JSONException
    {
        ArrayList<String> array = new ArrayList<>();
        array.add(jObject.optString("placas"));
        array.add(jObject.optString("color"));
        array.add(jObject.optString("marca"));
        return array;
    }

    public String getPlacas() throws JSONException
    { return jObject.getString("placas"); }

    public String getColor() throws JSONException
    { return jObject.getString("color"); }

    public String getMarca() throws JSONException
    { return jObject.getString("marca"); }

    public int getID() throws JSONException
    { return jObject.getInt("id"); }

    public boolean getIsEmpty(){
        return isEmpy;
    }
}
