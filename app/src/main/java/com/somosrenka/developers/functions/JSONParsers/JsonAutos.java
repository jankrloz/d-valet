package com.somosrenka.developers.functions.JSONParsers;

import android.os.Trace;
import android.util.Log;

import com.somosrenka.developers.models.Auto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
*CLASE PARA MANEJAR EL JSON DE RESPUESTA AL PEDIR LA LISTA DE AUTOS
*/
public class JsonAutos {
    public JSONArray jObjectArray;
    public JSONObject jObject;

    public JsonAutos(String jsonArray) throws JSONException {
        jObjectArray = new JSONArray(jsonArray);
    }

    public List<Auto> getAutos() throws JSONException {
        ArrayList<Auto> autosList = new ArrayList<>();
        String placa = "";
        String marca = "";
        String color = "";

        int len_ = jObjectArray.length();
        for (int i = 0; i < len_; i++) {
            jObject = jObjectArray.optJSONObject(i);
            Log.i("JsonAutos", "Obteniendo objeto JSON de un JSONARRAY");
            Log.i("JsonAutos", jObject.toString());
        }

        placa = jObject.optString("placas");
        color = jObject.optString("color");
        marca = jObject.optString("marca");
        autosList.add(new Auto(placa, color, marca));
        return autosList;
    }

    public List<String> getErrors() throws JSONException {
        ArrayList<String> array = new ArrayList<>();
        array.add(jObject.optString("placas"));
        array.add(jObject.optString("color"));
        array.add(jObject.optString("marca"));
        return array;
    }


    public String toString() {
        return this.jObjectArray.toString();
    }
}
