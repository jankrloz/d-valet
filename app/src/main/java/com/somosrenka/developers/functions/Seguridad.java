package com.somosrenka.developers.functions;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Seguridad {
    byte[] key;

    public void inizializar(String Llave) {
        try {
            byte[] keyStart = Llave.getBytes();
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            kgen.init(128, sr);
            SecretKey skey = kgen.generateKey();
            key = skey.getEncoded();
        } catch (Exception e) {
            Log.i("Seguridad", e.getMessage());

        }

    }

    public byte[] encriptarV2(byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;

    }

    public byte[] desencriptarV2(byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public String ByteToString(byte[] array) {


        try {
            String str = new String(array, "UTF-8");
            Log.i("Seguridad", str);
            return str;
        } catch (UnsupportedEncodingException e) {
            Log.i("Seguridad", e.getMessage());
            return null;
        }

        /*
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            buffer.append((char) array[i]);
        }
        Log.i("Seguridad", buffer.toString());
        return buffer.toString();*/
    }
}
