package com.somosrenka.developers.functions.JSONParsers;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.somosrenka.developers.models.Area;
import com.somosrenka.developers.models.AreasServicio;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thrashforner on 15/07/16.
 */
public class JsonAreaServicio
{
    private JSONObject jsonAreasServicio;
    private ArrayList<JsonArea> jsonArea;
    private AreasServicio areas;

    public JsonAreaServicio(String jsonObject) throws JSONException
    {
        this.jsonAreasServicio = new JSONObject(jsonObject);
        areas = new AreasServicio();
        this.initAreas();
    }

    //  Itera sobre el array principal para obtener todas las Areas
    public void initAreas() throws JSONException
    {
        Log.i("JsonAreaServicio","initAreas");
        Log.i("JsonAreaServicio", jsonAreasServicio.keys().toString());
        JSONObject area_temp = new JSONObject();
        JSONObject ubicacion_temp = new JSONObject();

        int  leng_ = jsonAreasServicio.length();
        Log.i("JsonAreaServicio","antes del primer FOR");
        for (int i=0; i<leng_ ; i++)
        {
            //OBTENEMOS UN AREA
            area_temp = jsonAreasServicio.getJSONObject("results");
            Log.i("JsonAreaServicio", area_temp.toString());
            //OTRO FOR PARA SACAR AHORA LAS UBICACIONES
        }
    }

    public void getCoords(JSONObject area) throws JSONException
    {
        int  leng_ = area.length();
        JSONObject ubicacion_temp = new JSONObject();
        ArrayList<LatLng> ubicaciones_tmp = new ArrayList<>();
        Log.i("JsonAreaServicio", "Antes del segundo for ");
        for (int i=0; i<leng_ ; i++)
        {
            //OBTENEMOS LAS UBICACIONES
            ubicacion_temp = jsonAreasServicio.getJSONObject("ubicacion");
            ubicaciones_tmp.add( new LatLng(ubicacion_temp.getDouble("latitud"), ubicacion_temp.getDouble("longitud")));
            Log.i("JsonAreaServicio", ubicacion_temp.toString());
        }
    }



}
