package com.somosrenka.developers.connection;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by thrashforner on 13/07/16.
 */
public class VolleyConnection
{
    //public static final String HOST = "http://192.168.1.72:8000/";
    public static final String HOST = "http://192.168.0.111:8000/";
    //final TextView mTextView = (TextView) findViewById(R.id.text);

    // Instantiate the RequestQueue.
    private RequestQueue queue;
    private String URL = HOST;

     public VolleyConnection(Context context)
     {
         this.queue = Volley.newRequestQueue(context);
     }

    public void makerequest()
    {
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                        Log.i("VolleyConecction", "Response is: " + response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyConecction", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
