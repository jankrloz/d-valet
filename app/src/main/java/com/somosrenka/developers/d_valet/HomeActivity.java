package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.somosrenka.developers.models.Ubicacion;
import com.somosrenka.developers.models.Usuario;

public class HomeActivity extends AppCompatActivity {
    Button Identificarme;
    Button Registrarme;
    Button Mapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //AGREGANDO ID DE TELEFONO
        /*
        try
        {
            if (checkPlayServices()) {
                gcm = GoogleCloudMessaging.getInstance(this);
                regid = getRegistrationId(context);

                if (regid.isEmpty()) {
                    registerInBackground();
                }
            } else
            {
                Log.i("HomeActivity", "No valid Google Play Services APK found.");
            }
        }
        catch(Exception e )
        {Log.i("HomeActivity",e.toString());}
        */

        try {
            Identificarme = (Button) findViewById(R.id.btn_registros);
            Registrarme = (Button) findViewById(R.id.btn_login);


            Identificarme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
            });

            Registrarme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, SignupActivity.class));
                }
            });

            Mapa = (Button) findViewById(R.id.btn_mapa);
            Mapa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, obtenerPosicion.class));
                }
            });

        } catch (Exception e) {
            Log.i("HomeActivity", e.toString());
            Log.i("HomeActivity", e.getMessage());
            Log.i("HomeActivity", e.getClass().toString());
        }

    }

}
