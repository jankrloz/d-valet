package com.somosrenka.developers.d_valet;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import android.Manifest;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.security.Permission;

public class MapsActivityCarlos extends FragmentActivity implements OnMapReadyCallback, OnConnectionFailedListener
{


    private GoogleMap mMap;
    private static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 2;
    private static final String[] PERMISO_FINE = {Manifest.permission.ACCESS_FINE_LOCATION};
    private static final String[] PERMISO_COARSE = {Manifest.permission.ACCESS_COARSE_LOCATION};

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i("MapsActivityCarlos", "OnCreate");
        // Assume thisActivity is the current activity
        //int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        setContentView(R.layout.activity_maps_activity_carlos);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        // if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {mMap.setMyLocationEnabled(true);} else {// Show rationale and request permission.}
        //int permiso_coarse_code = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        //CARGAMOS EL MAPA
        mapFragment.getMapAsync(this);

        /*
        if (permiso_coarse_code != PackageManager.PERMISSION_GRANTED) {
            requestPermission(PERMISO_COARSE, MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
        }
        */
        //pedimos los dos permisos
        /*
        //  AUTOCOMPLETE DE PLACES
        int PLACE_PICKER_REQUEST = 1;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try
        {startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);}
        catch (GooglePlayServicesRepairableException e)
        {Log.i("MapsActivityCarlos", e.toString());}
        catch (GooglePlayServicesNotAvailableException e)
        {Log.i("MapsActivityCarlos", e.toString());}


        autocompleteFragment.setOnPlaceSelectedListener(

                new PlaceSelectionListener()
        {
            @Override
            public void onPlaceSelected(ValetPlace place)
            {
                // TODO: Get info about the selected place.
                Log.i("MapsActivityCarlos", "ValetPlace: " + place.getName());
            }
            @Override
            public void onError(Status status)
            {
                // TODO: Handle the error.
                Log.i("MapsActivityCarlos", "An error occurred: " + status);
            }
        });
        Log.i("MapsActivityCarlos", "Se agrego autocompleteFragment");

        */

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();


        Log.i("MapsActivityCarlos", "Antes de crear PlaceAutocompleteFragment ");
        PlaceAutocompleteFragment autocompleteFragment  = (PlaceAutocompleteFragment) mapFragment.getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        Log.i("MapsActivityCarlos", "despues de PlaceAutocompleteFragment ");

        try
        {Log.i("MapsActivityCarlos", autocompleteFragment.toString());}
        catch(Exception e)
        {Log.i("MapsActivityCarlos", e.toString()); Log.i("MapsActivityCarlos", e.getClass().toString());}

        Log.i("MapsActivityCarlos", "TERMINO ONCREATE");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("MapsActivityCarlos", "ONMAPREADY");

        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng mexico = new LatLng(19.4303674, -99.1373904);
        mMap.addMarker(new MarkerOptions().position(mexico).title("Marcador en df"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(mexico));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mexico, 16)); //mover camara y ponerle un zoom
        //mMap.setOnInfoWindowClickListener(getInfoWindowClickListener());


        //      PEDIMOS PERMISOS
        int permiso_fine_code = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permiso_fine_code != PackageManager.PERMISSION_GRANTED)
            { requestPermission(PERMISO_FINE, MY_PERMISSIONS_REQUEST_FINE_LOCATION);}
        else
        {
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setMyLocationEnabled(true);
        }

        Log.i("MapsActivityCarlos", "antes de agregar InfoWindowClickListener");
        mMap.setOnInfoWindowClickListener(getInfoWindowClickListener());
        mMap.setOnMarkerClickListener(getMarkerClickListener());
        //mMap.setOnMyLocationButtonClickListener(getOnMyLocationButtonClickListener()); //OVERRIDE DE BOTON 'POSICION ACTUAL'

        Log.i("MapsActivityCarlos", "MyLocationButtonEnabled");
        //mMap.getUiSettings().setMyLocationButtonEnabled(true);
        String hash_ = "password";
        Log.i("MapsActivityCarlos", "TERMINO ONMAPREADY");
    }


    //METODO PARA OBTENER EL MARCADOR CLICKEADO
    public OnMarkerClickListener getMarkerClickListener() {
        Log.i("MapsActivityCarlos", "OnMarkerClickListener");
        return new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Log.i("MapsActivityCarlos", "@Override onMarkerClick");
                Toast.makeText(getApplicationContext(), "Click en el marcador ..." + marker.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            }
        };
    }

    public OnMyLocationButtonClickListener getOnMyLocationButtonClickListener()
    {
        Log.i("MapsActivityCarlos", "getInfoWindowClickListener");
        return new OnMyLocationButtonClickListener()
        {
            @Override
            public boolean onMyLocationButtonClick()
            {
                Log.i("MapsActivityCarlos", "@Override onMyLocationButtonClick");
                if (mMap.isMyLocationEnabled())
                {
                    Toast.makeText(getApplicationContext(), "MI HUBICACION ACTUAL HABILITADA" , Toast.LENGTH_SHORT).show();
                    return true;
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "HUBICACION ACTUAL DESHABILITADA" , Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        };

    }

    //METODO PARA MANEJAR CUANDO LE DA CLICK A LA INFORMACION DE UN MARCADOR
    public OnInfoWindowClickListener getInfoWindowClickListener() {
        Log.i("MapsActivityCarlos", "getInfoWindowClickListener");
        return new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.i("MapsActivityCarlos", "@Override onInfoWindowClick");
                Toast.makeText(getApplicationContext(), "Clicked a window with title..." + marker.getTitle(), Toast.LENGTH_SHORT).show();
            }
        };
    }


    private void requestPermission(String[] _permiso_, int request_code) {
        //ActivityCompat.requestPermissions(mActivity, new String[]{mManifestPersmission}, mRequestCode);
        ActivityCompat.requestPermissions(this, _permiso_, request_code);
    }

    private void promptSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme);
        builder.setTitle("TITULO");
        builder.setMessage("mDeniedNeverAskMsg *MENSAJE PARA PEDIRLE QUE ACTIVE LA UBICACION PARA PODER TRACKEARLO CUANDO LE HA DADO QUE NO QUIERE QUE LE PREGUNTEMOS NUNCA MAS*");
        builder.setPositiveButton("Ir a configuración", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                goToSettings();
                Log.i("MapsActivityCarlos","Termino gotoSettings");
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + this.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(myAppSettings);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.i("MapsActivityCarlos", "onRequestPermissionsResult");
        switch (requestCode)
        {
            /*
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION:
            {
                boolean hasSth = grantResults.length > 0;
                if (hasSth) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //user accepted , make call
                        Log.i("MapsAcptivityCarlos", "permiso concedido");
                        mMap.setMyLocationEnabled(true);

                    }
                    else if(grantResults[0] == PackageManager.PERMISSION_DENIED)
                    {
                        boolean should = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION);
                        if(should)
                        {
                            //user denied without Never ask again, just show rationale explanation
                            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppInvite_Preview);

                            builder.setTitle("Permiso denegado para COARSE LOCATION");
                            builder.setMessage("La aplicacion necesita del permiso de *COARSE LOCATION* para poder mostrarte los valets.Estas seguro de denegar el permiso?");
                            builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                    {dialog.dismiss(); }
                            });
                            builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                    requestPermission(PERMISO_COARSE, MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
                                }
                            });
                            builder.show();
                        }else
                        {
                            //user has denied with `Never Ask Again`, go to settings
                            promptSettings();
                        }
                    }
                }
            }// fin de coarse location
            */
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION:
            {
                boolean hasSth = grantResults.length > 0;
                if(hasSth)
                {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    {
                        //user accepted , make call
                        Log.i("MapsAcptivityCarlos","permiso concedido");
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        mMap.setMyLocationEnabled(true);
                    }
                    else if(grantResults[0] == PackageManager.PERMISSION_DENIED)
                    {
                        boolean should = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
                        if(should)
                        {
                            //user denied without Never ask again, just show rationale explanation
                            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppInvite_Preview);
                            builder.setTitle("Permiso denegado para FINE LOCATION");
                            builder.setMessage("La aplicacion necesita del permiso de HUBICACION para poder mostrarte los valets.Estas seguro de denegar el permiso?");
                            builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {dialog.dismiss();  }
                            });
                            builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                    requestPermission(PERMISO_FINE, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                                }
                            });
                            builder.show();
                        }else
                        {
                            //user has denied with `Never Ask Again`, go to settings
                            promptSettings();
                        }
                    }
                }
            }// fin de request FINE location
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        Log.i("MapsActivityCarlos","onConnectionFailed");

    }
}//fin de la clase
