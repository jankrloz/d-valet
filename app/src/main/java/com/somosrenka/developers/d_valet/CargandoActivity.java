package com.somosrenka.developers.d_valet;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.Archivo;
import com.somosrenka.developers.functions.JSONParsers.JsonDatosCarro;
import com.somosrenka.developers.functions.Seguridad;
import com.somosrenka.developers.models.Auto;
import com.somosrenka.developers.models.Usuario;

import org.json.JSONException;

import java.io.IOException;

public class CargandoActivity extends AppCompatActivity {
    Archivo archivo;
    Usuario usuario;
    Auto auto;
    private static final String DEBUG_TAG = "HttpExample";
    private EditText urlText;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargando);
        /*todo: empieza redireccion procedimental*/

        /*
        try {
            Log.i("Cargando", "se iniciar busqueda de usuaruio");
            archivo = new Archivo(this, "userLogin.txt");
            Log.i("Cargando", "se iniciar lectura de usuaruio");
            usuario = archivo.readUser();
            Log.i("Cargando", "se iniciar if de usuario");
            if (usuario != null) {
                Log.i("Cargando", "se intancia el usuario");
                try {
                    archivo = new Archivo(getApplicationContext(), "CarList.txt");
                    if (archivo.readAuto() != null) {
                        auto = archivo.readAuto();
                        Log.i("ListaAutos", "el archivo esta Correcto");
                    } else {
                        Log.i("ListaAutos", "el archivo esta vacio");
                        ActualizarArchivoCarros();
                    }
                } catch (Exception e) {
                    ActualizarArchivoCarros();
                }
                Usuario.getInstance(usuario.getEmail(), usuario.getToken());
                Intent mapa = new Intent(this, MapaFragment.class);
                mapa.putExtra("placas", auto.getPlaca());
                startActivity(mapa);
            } else {
                Log.i("Cargando", "no se pudo recuperar el usuario");
                Intent home = new Intent(this, HomeActivity.class);
                startActivity(home);
                finish();
            }
        } catch (Exception e) {
            Log.e("Cargando", e.toString());
        }
    }

    private void ActualizarArchivoCarros() {
        Log.i("ListaAutos", "Empezando Achivo local de autos");
        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            RESTConnection23 connection = null;
            connection = new RESTConnection23(RESTConnection23.HOST + "autos/");
            connection.setRequestMethod("GET");
            //Agregando la autenticacion a la peticion
            Log.i("ListaAutos", usuario.getToken().toString());
            connection.addTokenAuthentication(usuario.getToken().toString());
            //connection.setJsonRequest(); //indica que la peticion es de tipo json
            connection.setRequestDoInput(true);
            connection.setRequestDoOutput(true);

            Log.i("ListaAutos", "ANTES DE ENVIAR LA PETICION DE AUTOS");
            connection.CarListConnection();
            int status = connection.getStatus_();
            Log.i("ListaAutos", Integer.toString(status));
            //Decodificamos la respuesta con la claseJSon correspondiente
            JsonDatosCarro json_carro;
            if (connection.status200or201()) {
                Log.i("ListaAutos", "Peticion exitosa de lista de carros");
                json_carro = new JsonDatosCarro(connection.getResponse());
                Log.i("ListaAutos", json_carro.toString());
                try {
                    if (json_carro.getIsEmpty()) {
                        Intent redireccion = new Intent(CargandoActivity.this, AddCarActivity.class);
                        redireccion.putExtra("token", usuario.getToken());
                        startActivity(redireccion);
                        finish();
                    } else {
                        archivo = new Archivo(getApplicationContext(), "CarList.txt");
                        auto = new Auto(json_carro.getID(), json_carro.getPlacas(), json_carro.getColor(), json_carro.getMarca());
                        archivo.writeCar(auto);
                    }
                } catch (Exception e) {
                    Intent redireccion = new Intent(CargandoActivity.this, AddCarActivity.class);
                    redireccion.putExtra("token", usuario.getToken());
                    startActivity(redireccion);
                    finish();
                }

            } else {
                Intent redireccion = new Intent(CargandoActivity.this, AddCarActivity.class);
                redireccion.putExtra("token", usuario.getToken());
                startActivity(redireccion);
                finish();
            }
        } catch (IOException ex) {
            Log.i("ListaAutos", ex.toString());
        } catch (JSONException ex) {
            Log.i("ListaAutos", ex.toString());

        }
        Log.i("ListaAutos", "Terminando de actulizar archivo");
        //*/
        /*Todo: termina redireccion procidemental*/
        /*Todo: empieza redireccion asincrona*/
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                new NanozTask().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(CargandoActivity.this, "No hay conecion a internet", Toast.LENGTH_LONG).show();
            finish();
        }


    }

    private class NanozTask extends AsyncTask<String, String, String>
    {
        @Override
        protected String doInBackground(String... string) {
            try {
                publishProgress("Empezamos a buscar si existe algun usuario registrado");
                Archivo archivo = new Archivo(getApplicationContext(), "userLogin.txt");
                Usuario usuario = archivo.readUser();
                if (usuario != null) {
                    publishProgress("Encontramos un usuario registrado");
                    Usuario.getInstance(usuario.getEmail(), usuario.getToken(), usuario.getPassword());
                    publishProgress("Ahora verificaremos que el usuario sea valido");
                    RESTConnection23 connection = new RESTConnection23(RESTConnection23.HOST + "rest-auth/login/");
                    connection.setRequestMethod("POST");
                    connection.addRequestHeader("Content-Type", "application/json; charset=UTF-8");
                    Seguridad seguridad = new Seguridad();
                    seguridad.inizializar(usuario.getEmail());
                    byte[] paso1 = seguridad.desencriptarV2(usuario.getPassword());
                    String paso2 = seguridad.ByteToString(paso1);
                    Log.i("Cargando",paso2);
                    Log.i("Cargando",usuario.getEmail());
                    Log.i("Cargando",usuario.getEmail());
                    connection.addRequestParam("password", paso2);
                    connection.addRequestParam("username", usuario.getEmail());
                    connection.addRequestParam("email", usuario.getEmail());
                    connection.setRequestDoInput(true);
                    connection.setRequestDoOutput(true);
                    connection.LoginConnection();
                    if (connection.status200or201()) {
                        publishProgress("Usuario valido");
                        try {
                            publishProgress("Veremos si tiene algun carro");
                            archivo = new Archivo(getApplicationContext(), "CarList.txt");
                            if (archivo.readAuto() != null) {
                                publishProgress("Carro encontrado");
                                auto = archivo.readAuto();
                                return "3";
                            } else {
                                publishProgress("Veremos si tiene algun carro");
                                return ActualizarArchivoCarros(usuario);
                            }
                        } catch (Exception e) {
                            return ActualizarArchivoCarros(usuario);
                        }
                    } else {
                        publishProgress("Usuario no valido");
                        return "2";
                    }
                } else {
                    publishProgress("No encontramos un usuario registrado");
                    return "1";
                }
            } catch (Exception e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        private String ActualizarArchivoCarros(Usuario usuario) {
            try {
                RESTConnection23 connection = null;
                connection = new RESTConnection23(RESTConnection23.HOST + "autos/");
                connection.setRequestMethod("GET");
                connection.addTokenAuthentication(usuario.getToken());
                connection.setRequestDoInput(true);
                connection.setRequestDoOutput(true);
                connection.CarListConnection();
                JsonDatosCarro json_carro;
                if (connection.status200or201()) {
                    json_carro = new JsonDatosCarro(connection.getResponse());
                    try {
                        if (!json_carro.getIsEmpty()) {
                            archivo = new Archivo(getApplicationContext(), "CarList.txt");
                            auto = new Auto(json_carro.getID(), json_carro.getPlacas(), json_carro.getColor(), json_carro.getMarca());
                            archivo.writeCar(auto);
                            return "3";
                        }
                    } catch (Exception e) {
                        Log.i("Cargando", e.toString());
                    }
                }
            } catch (IOException ex) {
                Log.i("Cargando", ex.toString());
            } catch (JSONException ex) {
                Log.i("Cargando", ex.toString());
            }
            return "2";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            switch (result) {
                case "1":
                    startActivity(new Intent(CargandoActivity.this, HomeActivity.class));
                    break;
                case "2":
                    startActivity(new Intent(CargandoActivity.this, AddCarActivity.class));
                    break;
                case "3":
                    startActivity(new Intent(CargandoActivity.this, MapsActivity_.class));
                    break;
                default:
                    startActivity(new Intent(CargandoActivity.this, LoginActivity.class));
            }
            finish();
        }
    }

}
